# UpdateTUJ.py
# (c) 2011-2014 by Jeff Grimmett
# Freely re-distributable for all non-commercial uses
# including modification and improvement.
#
# Shine on you crazy diamond.
 ################################################################

# See http://wowblog.grimmlabs.com/archives/894 for
# instructions on using this script.

# Here are some variables you will need to change.
#
# Your key is taken from the URL that TUJ give you, and is the part that
# follows 'key=' and ends at (but does not include) '?'
myKey = 'OdnyaGy1zC39nnUH7UwMSyRUQdTSpj64'

# List here all the realms you are interested in. You can get more than one
# if you wish.
#
# Only one realm per line, followed by a comma, please
realms = [
    'A-Alleria',
]

# The location of your addon directory
TargetPath  = 'D:\\Programs\\World of Warcraft\\interface\\addons'

#
# That's the end of the user-modified part of this app. Everything else is best
# left to the coders.
#
# I comment it in case you're one of them.
#

# The base URL for TUJ
TUJURL = 'https://theunderminejournal.com/TheUndermineJournal.zip'
# Parameters to pass in the URL
TUJPARMS = {'key': myKey, 'realms':','.join(realms),}

# Note: this can only be used ten times a day. After that, it
# goes boom, but it won't do any damage when it does.

import os
import os.path
import shutil
import zipfile

import requests

# The name of the zip file, locally
LocalFile                      = 'TheUndermineJournal.ZIP'
# The final path of the addon
FinalDestination = os.path.join(TargetPath, 'TheUndermineJournal')

print "Starting ..."
print "Opening %s" % TUJURL

# Open the URL and local file
r = requests.get(TUJURL, params=TUJPARMS)

#
# By the way, if  you don't use 'requests' instead of urllib2, you're
# just being hard on yourself for no good reason.
#
# pip install requests
#
# do eet
#
# JUST DOO EEET
#

if r.status_code == 200:
    print "File downloaded."

    # Oh looky I figured out context managers. Huzzah.
    with file(LocalFile, 'wb') as f:
        f.write(r.content)

    # Make sure we have a place to unpack
    if not os.path.exists('unpack'):
        print "Creating unpacking space"
        os.mkdir('unpack')
    else:
        print "Cleaning up last session"
        shutil.rmtree('unpack', True)
        os.mkdir('unpack')

    # Now that the file is downloaded, we unpack it.
    print "Unpacking ...",
    z = zipfile.ZipFile(LocalFile)
    z.extractall('unpack')
    z.close()
    print "Done!"

    # Now that the directory is unpacked, we delete the old TUJ addon
    if os.path.exists(FinalDestination):
        print "Deleting old addon"
        print FinalDestination
        shutil.rmtree(FinalDestination, True)

    # Now that the old TUJ is gone, this one is put in its place.
    print "Installing updated version ...",
    shutil.copytree('unpack/TheUndermineJournal', FinalDestination)

else:
    print "Download failed - error code [%d]" % r.status_code


# All done!
print "All done!"

raw_input("Press ENTER to continue.")
